<?php 

	$contacts = [
		'contact1' => [
			'nom' => 'FLEURET',
			'prenom' => 'Nathan',
			'age' => 29,
			'metier' => 'Développeur'
		],
		'contact2' => [
			'nom' => 'BRIAND',
			'prenom' => 'Aristide',
			'age' => 15,
			'metier' => 'Etudiant'
		],
		'contact3' => [
			'nom' => 'CHARPENTIER',
			'prenom' => 'Jean-Jacques',
			'age' => 35,
			'metier' => 'Couvreur'
		]
	];

	//echo $contacts['contact1']['prenom'];
	echo 'Voici les contacts majeurs:'."\n";
	foreach ($contacts as $numcontact) {
			if ($numcontact['age'] >= 18) {
				echo $numcontact['prenom'].' '.$numcontact['age']."\n";
			}
			
	}
?>